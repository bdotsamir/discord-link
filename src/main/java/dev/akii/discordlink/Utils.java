package dev.akii.discordlink;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Utils {

  public static void initConfig(@NotNull DiscordLink plugin) {
    FileConfiguration config = plugin.getConfig();

    config.addDefault("discord.token", "");
    config.addDefault("discord.prefix", ">");
    config.addDefault("bedrockAPIKey", "");
    config.addDefault("debugMode", false);

    config.options().copyDefaults(true);
    plugin.saveConfig();
  }

  public static @NotNull String formatUUID(@NotNull String unformatted) {
    if(unformatted.contains("-")) return unformatted;

    return unformatted.substring(0, 8) + "-" // 8 characters
            + unformatted.substring(8, 12) + "-" // 4 characters
            + unformatted.substring(12, 16) + "-" // 4 characters
            + unformatted.substring(16, 20) + "-" // 4 characters
            + unformatted.substring(20, 32); // 12 characters
  }

  // Essentially a clone of the config class from JavaPlugin,
  // but this is for mapping usernames to IDs provided by Lambot
  public static class CustomConfig {
    private final String fileName;
    private final JavaPlugin plugin;

    private final File configFile;
    private FileConfiguration fileConfiguration;

    public CustomConfig(@NotNull JavaPlugin plugin, @NotNull String fileName) {
      this.plugin = plugin;
      this.fileName = fileName;
      this.configFile = new File(plugin.getDataFolder(), fileName);
    }

    public void reloadConfig() {
      fileConfiguration = YamlConfiguration.loadConfiguration(configFile);

      // Look for defaults in the jar
      InputStream defConfigStream = plugin.getResource(fileName);
      if (defConfigStream != null) {
        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream));
        fileConfiguration.setDefaults(defConfig);
      }
    }

    public FileConfiguration getConfig() {
      if (fileConfiguration == null) {
        this.reloadConfig();
      }
      return fileConfiguration;
    }

    public void saveConfig() {
      if (fileConfiguration != null || configFile != null) {
        try {
          getConfig().save(configFile);
        } catch (IOException ex) {
          plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
      }
    }

    public void saveDefaultConfig() {
      if (!configFile.exists()) {
        this.plugin.saveResource(fileName, false);
      }
    }
  }

  public static void initWhitelist(@NotNull DiscordLink plugin) {
    CustomConfig whitelist = plugin.getWhitelist();
    FileConfiguration config = whitelist.getConfig();

    config.options().copyDefaults(true);
    whitelist.saveConfig();
  }

  public static class SecondsFormatter {
    public static @NotNull String format(int seconds) {
      String[] words = {"year", "day", "hour", "minute", "second"};
      int[] values = {365 * 24 * 60 * 60, 24 * 60 * 60, 60 * 60, 60, 1};
      List<String> result = new ArrayList<>();

      for (int i = 0; i < words.length; i++) {
        int temp = (int) Math.floor((float) seconds / (float) values[i]);
        if (temp > 0) {
          result.add(temp + " " + getPlural(temp, words[i]));
        }

        seconds %= values[i];  // remainder of seconds after dividing by value at index i
      }

      String fin = String.join(" ", result);  // join the strings in the list with a space between each string
      return fin.equals("") ? "0 seconds" : fin;
    }

    private static String getPlural(int number, String word) {   // helper method to return the correct plural form of a word based on number passed in as parameter
      return number == 1 ? word : word + 's';   // if number is 1 then just return the singular form of the word otherwise add an 's' to it to make it plural
    }
  }

}
