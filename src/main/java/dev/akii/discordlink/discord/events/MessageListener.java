package dev.akii.discordlink.discord.events;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.discord.DiscordCommandInterface;
import dev.akii.discordlink.discord.Lambot;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;

public class MessageListener extends ListenerAdapter {

  private final DiscordLink plugin;
  private final Lambot bot;
  private final HashMap<String, DiscordCommandInterface> commands;

  public MessageListener(DiscordLink plugin, Lambot bot, HashMap<String, DiscordCommandInterface> commands) {
    this.plugin = plugin;
    this.bot = bot;
    this.commands = commands;
  }

  @Override
  public void onMessageReceived(@NotNull MessageReceivedEvent event) {
    // We don't want to respond to other bot accounts, including ourselves
    if (event.getAuthor().isBot()) return;

    Message message = event.getMessage();
    // getContentRaw() is an atomic getter
    // getContentDisplay() is a lazy getter which modifies the content for e.g. console view (strip discord formatting)
    String content = message.getContentRaw();

    String prefix = this.plugin.getConfig().getString("discord.prefix");
    if (prefix == null) {
      plugin.getLogger().severe("config.yml value discord.prefix should not be null.");
      return;
    }
    // If the message does not begin with the prefix, return
    if (!content.startsWith(prefix))
      return;

    // If the command was run in DMs, return
    if (!event.getChannelType().isGuild()) {
      event.getChannel().sendMessage(":x: **Sorry!** It isn't possible to run commands from DMs.").queue();
      return;
    }

    String[] splitContent = content.split(prefix)[1].split(" ");
    String commandName = splitContent[0];
    String[] args = Arrays.copyOfRange(splitContent, 1, splitContent.length);

    // And finally run the command :)
    commands.get(commandName).run(plugin, bot, message, args);
  }
}
