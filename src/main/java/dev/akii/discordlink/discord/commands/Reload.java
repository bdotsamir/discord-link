package dev.akii.discordlink.discord.commands;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.discord.DiscordCommandInterface;
import dev.akii.discordlink.discord.Lambot;
import net.dv8tion.jda.api.entities.Message;

public class Reload implements DiscordCommandInterface {

  public void run(DiscordLink plugin, Lambot bot, Message message, String[] args) {
    plugin.reloadConfig();
    plugin.getWhitelist().reloadConfig();
    message.getChannel().sendMessage(":white_check_mark: **`config.yml` was reloaded!**").queue();
  }

}
