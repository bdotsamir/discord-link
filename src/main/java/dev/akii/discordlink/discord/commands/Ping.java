package dev.akii.discordlink.discord.commands;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.discord.DiscordCommandInterface;
import dev.akii.discordlink.discord.Lambot;
import net.dv8tion.jda.api.entities.Message;
import org.jetbrains.annotations.NotNull;

public class Ping implements DiscordCommandInterface {

  public void run(DiscordLink plugin, @NotNull Lambot bot, Message message, String[] args) {
    bot.getAPI().getRestPing().queue((time) -> message.getChannel().sendMessageFormat(":ping_pong: **Pong!** `%dms`", time).queue());
  }

}
