package dev.akii.discordlink.discord.commands;

import com.google.gson.Gson;
import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.Utils;
import dev.akii.discordlink.discord.DiscordCommandInterface;
import dev.akii.discordlink.discord.Lambot;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import org.geysermc.floodgate.api.FloodgateApi;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

public class Whitelist implements DiscordCommandInterface {

  public void run(DiscordLink plugin, Lambot bot, @NotNull Message message, String[] args) {
    Member member = message.getMember();
    if (member == null) {
      message.getChannel().sendMessage(":x: **There was an error.** I couldn't get your Discord profile. This is likely a caching issue. Try again in a few minutes.").queue();
      return;
    }

    String minecraftUsername;
    try {
      minecraftUsername = args[0];
    } catch (ArrayIndexOutOfBoundsException e) {
      message.getChannel().sendMessage(":x: **Missing Minecraft username to whitelist.**").queue();
      return;
    }

    plugin.getLogger().info(minecraftUsername);

    FloodgateApi floodgateApi = FloodgateApi.getInstance();
    String floodgatePlayerPrefix = floodgateApi.getPlayerPrefix();

    String uuidToWhitelist;
    String discordID = message.getAuthor().getId();
    Gson gson = new Gson();

    String bedrockAPIKey = plugin.getConfig().getString("bedrockAPIKey");

    // If bedrock user,
    if (minecraftUsername.startsWith(floodgatePlayerPrefix)) {
      plugin.getLogger().info("Detected Bedrock username. Generating XUID -> UUID");
      message.getChannel().sendMessage(":moyai: **Bedrock user detected!** Getting XUID...").queue();

      // Get it without the bedrock prefix
      String withoutPrefix = minecraftUsername.substring(1);
      plugin.getLogger().fine(withoutPrefix);

      String urlToSend = "https://mcprofile.io/api/v1/bedrock/gamertag/" + withoutPrefix;
      HttpURLConnection request;
      int responseCode;

      try {
        URI uri = new URI(urlToSend);
        URL url = uri.toURL();
        request = (HttpURLConnection) url.openConnection();
        request.setRequestProperty("Content-Type", "application/json");
        request.setRequestProperty("x-api-key", bedrockAPIKey);

        request.connect();
        responseCode = request.getResponseCode();
      } catch (Exception e) {
        message.getChannel().sendMessage(":x: **There was an error.** Your username may have been spelled incorrectly, or the MCProfile API is currently experiencing too many requests. Please try again later.").queue();

        e.printStackTrace();
        return;
      }

      try {
        InputStreamReader inputStreamReader = new InputStreamReader((InputStream) request.getContent());
        BedrockPlayerJSON playerJSON = gson.fromJson(inputStreamReader, BedrockPlayerJSON.class);

        uuidToWhitelist = Utils.formatUUID(playerJSON.floodgateuid);

      } catch (Exception e) {
        switch(responseCode) {
          case 400: {
            message.getChannel().sendMessage(":x: **There was an error.** The data being sent to the MCProfile API was malformed. Please contact Strange.").queue();
            break;
          }
          case 404: {
            message.getChannel().sendMessage(":x: **There was an error.** The username could not be found, or the API endpoint changed.").queue();
            break;
          }
          case 429: {
            message.getChannel().sendMessage(":x: **There was an error.** The MCProfile API is currently experiencing too many requests. Please try again in a few moments.").queue();
            break;
          }
          case 500: // fallthrough
          case 503: {
            message.getChannel().sendMessage(":x: **There was an error.** MCProfile's servers are currently malfunctioning. Please try again later.").queue();
            break;
          }
          default: {
            message.getChannel().sendMessage(":x: **There was an error.** I couldn't properly deserialize the data coming back from Mojang. Please contact Strange.").queue();
            break;
          }
        }

        e.printStackTrace();
        return;
      }

    } else { // else, java user.
      String urlToSend = "https://api.mojang.com/users/profiles/minecraft/" + minecraftUsername + "?at=" + System.currentTimeMillis() / 1000L;

      message.getChannel().sendMessage(":gear: **Getting Java UUID...**").queue();

      HttpURLConnection request;
      int responseCode;

      try {
        URI uri = new URI(urlToSend);
        URL url = uri.toURL();
        request = (HttpURLConnection) url.openConnection();
        request.setRequestProperty("Content-Type", "application/json");

        // Send the request
        request.connect();
        responseCode = request.getResponseCode();
      } catch(Exception e) {
        message.getChannel().sendMessage(":x: **There was an error.** Your username may have been spelled incorrectly, or Mojang is currently experiencing too many requests. Please try again later.\n\n*If you are a bedrock user, please run the command again with a " + floodgatePlayerPrefix + " in front of your username.*").queue();

        e.printStackTrace();
        return;
      }

      try {
        InputStreamReader inputStreamReader = new InputStreamReader((InputStream) request.getContent());
        JavaPlayerJSON playerJSON = gson.fromJson(inputStreamReader, JavaPlayerJSON.class);

        uuidToWhitelist = Utils.formatUUID(playerJSON.id);

      } catch(Exception e) {
        switch(responseCode) {
          case 400: {
            message.getChannel().sendMessage(":x: **There was an error.** The data being sent to Mojang was malformed. Please contact Strange.").queue();
            break;
          }
          case 404: {
            message.getChannel().sendMessage(":x: **There was an error.** The username could not be found, or the API endpoint changed.").queue();
            break;
          }
          case 429: {
            message.getChannel().sendMessage(":x: **There was an error.** Mojang is currently experiencing too many requests. Please try again in a few moments.").queue();
            break;
          }
          case 500: // fallthrough
          case 503: {
            message.getChannel().sendMessage(":x: **There was an error.** Mojang's servers are currently malfunctioning. Please try again later.").queue();
            break;
          }
          default: {
            message.getChannel().sendMessage(":x: **There was an error.** I couldn't properly deserialize the data coming back from Mojang. Please contact Strange.").queue();
            break;
          }
        }

        e.printStackTrace();
        return;
      }

    }

    Utils.CustomConfig whitelist = plugin.getWhitelist();

    String user = whitelist.getConfig().getString(uuidToWhitelist);
    if (user != null) {
      message.getChannel().sendMessage(":warning: **This account has already been whitelisted!.**").queue();
      return;
    }

    whitelist.getConfig().addDefault(uuidToWhitelist + ".discordID", discordID);
    whitelist.saveConfig();

    message.getChannel().sendMessage("✅ **Successfully whitelisted** `" + minecraftUsername + "` (`" + uuidToWhitelist + "`)").queue();
  }

  // This class provides a source for deserialization the JSON we receive from Mojang
  private static class JavaPlayerJSON {
    public String name;
    public String id;
    @Nullable public String path;

    // No-args constructor
    public JavaPlayerJSON() { }
  }

  private static class BedrockPlayerJSON {
    public String gamertag;
    public String xuid;
    public String floodgateuid;
    public boolean linked;
    public String java_uuid;
    public String java_name;

    public BedrockPlayerJSON() { }
  }

}
