package dev.akii.discordlink.discord;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.discord.commands.*;
import dev.akii.discordlink.discord.events.MessageListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class Lambot {

  private final DiscordLink plugin;
  private JDA api;
  private HashMap<String, DiscordCommandInterface> commands;

  public Lambot(@NotNull DiscordLink plugin) {

    this.plugin = plugin;

    try {
      String token = plugin.getConfig().getString("discord.token");

      this.commands = new HashMap<>();
      commands.put("ping", new Ping());
      commands.put("whitelist", new Whitelist());
      commands.put("reload", new Reload());
      commands.put("getuuid", new GetUUID());

      MessageListener messageListener = new MessageListener(this.plugin, this, this.commands);

      // export the JDA object
      this.api = JDABuilder.createDefault(token,
                      GatewayIntent.GUILD_MEMBERS,
                      GatewayIntent.GUILD_MESSAGES)
              .addEventListeners(messageListener)
              .build().awaitReady();
    } catch (Exception e) {
      plugin.getLogger().severe("Bot could not be initialized: " + e);
    }
  }

  public DiscordLink getPlugin() {
    return this.plugin;
  }

  public JDA getAPI() {
    return this.api;
  }

  public HashMap<String, DiscordCommandInterface> getCommands() {
    return this.commands;
  }
}
