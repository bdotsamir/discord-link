package dev.akii.discordlink.discord;

import dev.akii.discordlink.DiscordLink;
import net.dv8tion.jda.api.entities.Message;

public interface DiscordCommandInterface {

  void run(DiscordLink plugin, Lambot bot, Message message, String[] args);

}
