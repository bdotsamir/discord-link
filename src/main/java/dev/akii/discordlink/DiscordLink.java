package dev.akii.discordlink;

import dev.akii.discordlink.discord.Lambot;
import dev.akii.discordlink.minecraft.commands.*;
import dev.akii.discordlink.minecraft.events.PlayerKickListener;
import dev.akii.discordlink.minecraft.events.PlayerLoginListener;
import net.dv8tion.jda.api.entities.Guild;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class DiscordLink extends JavaPlugin {

  private Lambot lambot;
  private Utils.CustomConfig whitelist;

  @Override
  public void onEnable() {
    getLogger().info("onEnable called :)");

    getLogger().info("Booting the bot...");
    this.lambot = new Lambot(this);

    // dev.akii.discordlink.Utils - collection of static methods
    Utils.initConfig(this);

    // Initialize the plugin whitelist linker
    this.whitelist = new Utils.CustomConfig(this, "whitelist.yml");
    Utils.initWhitelist(this);

    // Register all the minecraft commands
    getLogger().info("Registering commands...");
    initializeMinecraftCommands();

    getLogger().info("Registering listeners...");
    PlayerLoginListener loginListener = new PlayerLoginListener(this);
    PlayerKickListener kickListener = new PlayerKickListener(this);
    getServer().getPluginManager().registerEvents(loginListener, this);
    getServer().getPluginManager().registerEvents(kickListener, this);

    if (getServer().hasWhitelist()) {
      getLogger().warning("Warning: Whitelist is enabled on this server. This is something that DiscordLink provides as an alternative.");
      getLogger().warning("That said, if a player is already on the server's whitelist, this plugin will let them through regardless of if they've linked their account.");
//      getLogger().warning("You can change this behaviour by modifying the config.");
    }

    getLogger().info("Done");
  }

  @Override
  public void onDisable() {
    getLogger().info("onDisable called :)");

    this.lambot.getAPI().shutdown();
  }

  private void initializeMinecraftCommands() {
    ArrayList<Command> commandList = new ArrayList<>();

    commandList.add(new SetLogger(this));
    commandList.add(new ReloadConfig(this));
    commandList.add(new About(this));

    System.out.println(commandList);

//    commandList.forEach((command) -> {
//      command.permissionMessage(MCResponse.getPrefix().append(Component.text("You do not have access to this command!", NamedTextColor.RED)));
//    });

    CommandMap commandMap = this.getServer().getCommandMap();
    commandMap.registerAll("discordlink", commandList);
  }

  public boolean getDebugMode() {
    FileConfiguration config = this.getConfig();

    return config.getBoolean("debugMode");
  }

  public Lambot getBot() {
    return this.lambot;
  }

  public Utils.CustomConfig getWhitelist() {
    return this.whitelist;
  }

}
