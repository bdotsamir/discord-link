package dev.akii.discordlink.minecraft.events;

import dev.akii.discordlink.DiscordLink;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

import java.util.UUID;

public class PlayerKickListener implements Listener {

  private final DiscordLink plugin;

  public PlayerKickListener(DiscordLink plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onPlayerKick(PlayerKickEvent event) {
    // If the server doesn't have a whitelist, stop.
    if(!event.getPlayer().getServer().hasWhitelist()) return;

    // If the player is not being kicked for not being on the whitelist, stop.
    if(event.getCause() != PlayerKickEvent.Cause.WHITELIST) return;

    Player player = event.getPlayer();
    UUID uuid = player.getUniqueId();

    boolean isOnPluginWhitelist = plugin.getWhitelist().getConfig().getString(uuid.toString()) != null;

    // If the player is not on the server whitelist but *is* on the plugin whitelist,
    // allow them through (cancel the kick event).
    if(isOnPluginWhitelist) {
      plugin.getLogger().info("Player is not on the server whitelist, but is on the plugin whitelist. Allowing.");
      event.setCancelled(true);
      return;
    }
  }
}
