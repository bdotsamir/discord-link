package dev.akii.discordlink.minecraft.events;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.Utils;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.jetbrains.annotations.NotNull;

public class PlayerLoginListener implements Listener {

  private final DiscordLink plugin;

  public PlayerLoginListener(@NotNull DiscordLink plugin) {
    this.plugin = plugin;
  }

  @EventHandler()
  public void onPlayerJoin(PlayerLoginEvent event) {

    // Get the pre-this-event-listener reason
    PlayerLoginEvent.Result reasonForPreCheck = event.getResult();
    // If the reason was anything other than getting kicked because
    // they weren't on the server whitelist,
    if (reasonForPreCheck != PlayerLoginEvent.Result.ALLOWED
            && reasonForPreCheck != PlayerLoginEvent.Result.KICK_WHITELIST) {
      event.disallow(event.getResult(), event.kickMessage());
      return;
    }
    // is the above code correct?
    // if the player is allowed, run the code
    // if the player is banned, don't run the code
    // if the server is full, don't run the code
    // if the player is not on the server whitelist, run the code
    // if there is another error, don't run the code.

    Player player = event.getPlayer();
    String uuid = player.getUniqueId().toString();

    // If the server has a whitelist and the player is on it,
    // allow them through.
    if (event.getPlayer().getServer().hasWhitelist() && player.isWhitelisted()) {
      plugin.getLogger().info("Allowing " + player.getName() + " into the server because they are already whitelisted via server's whitelist, not the plugin's.");
      event.allow();

      return;
    }

    Utils.CustomConfig customWhitelist = plugin.getWhitelist();

    String uuidInWhitelist = customWhitelist.getConfig().getString(uuid);

    TextComponent kickMessage = Component.text("You have not been whitelisted yet!", NamedTextColor.RED)
            .decoration(TextDecoration.BOLD, false)
            .append(Component.text("\n\n"))
            .append(Component.text("Please run\n", NamedTextColor.WHITE))
            .append(Component.text(">whitelist " + event.getPlayer().getName(), NamedTextColor.AQUA, TextDecoration.BOLD))
            .append(Component.text("\nin the whitelisting channel.", NamedTextColor.WHITE));

    // Kick the player because they aren't whitelisted.
    if (uuidInWhitelist == null) {
      event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, kickMessage);
      return;
    }

    plugin.getLogger().info("Allowed user " + event.getPlayer().getName() + " into the server.");
    event.allow();
  }

}
