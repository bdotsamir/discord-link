package dev.akii.discordlink.minecraft;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class CompositeCommand extends Command {

  private boolean includeConsole = true; // whether the console can run this command
  private boolean isPermissionRequired = true; // whether you need the permission for the command in order to *run* the command

  protected CompositeCommand(@NotNull String name, @NotNull String description, @NotNull String usageMessage, @NotNull List<String> aliases) {
    super(name, description, usageMessage, aliases);
  }

  @Override
  public boolean execute(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
    this.setup(); // Call the setup method which brings in permissions and such

    if (this.getPermission() != null && !sender.hasPermission(this.getPermission()) && !sender.isOp() && this.isPermissionRequired()) {
      sender.sendMessage(MCResponse.error("You do not have access to this command!"));
      return false;
    }
    return this.call(sender, args);
  }

  // Permissions and such go here.
  public abstract void setup();

  public abstract boolean call(@NotNull CommandSender sender, @NotNull String[] args);

  public boolean isConsoleIncluded() {
    return includeConsole;
  }

  public void setConsoleIncluded(boolean includeConsole) {
    this.includeConsole = includeConsole;
  }

  public boolean isPermissionRequired() {
    return isPermissionRequired;
  }

  public void setPermissionRequired(boolean permissionRequired) {
    isPermissionRequired = permissionRequired;
  }
}
