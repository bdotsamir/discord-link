package dev.akii.discordlink.minecraft.commands;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.minecraft.CompositeCommand;
import dev.akii.discordlink.minecraft.MCResponse;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class About extends CompositeCommand {

  private final DiscordLink plugin;

  public About(DiscordLink plugin) {
    super("about", "About the plugin", "/<command>", new ArrayList<>());
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("discordlink.about");
    this.setPermissionRequired(false);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, String[] args) {
    Component message = MCResponse.getPrefix()
            .append(Component.text("DiscordLink", NamedTextColor.RED)
                    .append(Component.text(" - ", NamedTextColor.GRAY))
                    .append(Component.text("Version: v" + plugin.getDescription().getVersion() + "\n", NamedTextColor.WHITE)))
            .append(Component.text(" - ", NamedTextColor.GRAY))
            .append(Component.text("Created by:", NamedTextColor.WHITE))
            .append(Component.text(" Strangnessness ", NamedTextColor.AQUA))
            .append(Component.text("(@StrangenessLive)\n", NamedTextColor.WHITE))
            .append(Component.text(" - ", NamedTextColor.GRAY))
            .append(Component.text("Allows players to whitelist other players via Discord.\n", NamedTextColor.WHITE));

    sender.sendMessage(message);
    return true;
  }

}
