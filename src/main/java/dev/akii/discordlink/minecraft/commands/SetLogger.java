package dev.akii.discordlink.minecraft.commands;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.minecraft.CompositeCommand;
import dev.akii.discordlink.minecraft.MCResponse;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SetLogger extends CompositeCommand {

  private final DiscordLink plugin;

  public SetLogger(DiscordLink plugin) {
    super("setlogger", "Sets the internal logging level of the plugin", "<command> <logger level>", new ArrayList<>());
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("discordlink.setlogger");
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    String level;
    try {
      level = args[0];
    } catch (ArrayIndexOutOfBoundsException e) {
      sender.sendMessage(MCResponse.error("Missing the logger level. Must be one of FINE, INFO, WARNING, SEVERE"));
      return false;
    }

    Logger logger = plugin.getLogger();

    switch (level.toLowerCase()) {
      case "fine" -> logger.setLevel(Level.FINE);
      case "info" -> logger.setLevel(Level.INFO);
      case "warning" -> logger.setLevel(Level.WARNING);
      case "severe" -> logger.setLevel(Level.SEVERE);
      default -> {
        sender.sendMessage(MCResponse.error("Level must be one of FINE, INFO, WARNING, SEVERE"));
        return false;
      }
    }

    sender.sendMessage(MCResponse.success("Successfully set the logger level to " + level.toLowerCase()));

    return true;
  }

  @Override
  public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, String[] args) {
    ArrayList<String> list = new ArrayList<>();
    list.add("fine");
    list.add("info");
    list.add("warning");
    list.add("severe");

    return list;
  }

}
