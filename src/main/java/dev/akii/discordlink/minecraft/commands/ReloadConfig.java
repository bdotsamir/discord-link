package dev.akii.discordlink.minecraft.commands;

import dev.akii.discordlink.DiscordLink;
import dev.akii.discordlink.minecraft.CompositeCommand;
import dev.akii.discordlink.minecraft.MCResponse;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ReloadConfig extends CompositeCommand {

  private final DiscordLink plugin;

  public ReloadConfig(DiscordLink plugin) {
    super("reloadconfig", "Reloads all files in the plugin's folder", "<command>", new ArrayList<>());
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("discordlink.reloadconfig");
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    // reload *all* the configs.
    plugin.reloadConfig();
    plugin.getWhitelist().reloadConfig();

    sender.sendMessage(MCResponse.success("Reloaded all configs"));

    return true;
  }
}
