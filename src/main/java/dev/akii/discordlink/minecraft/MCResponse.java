package dev.akii.discordlink.minecraft;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

public class MCResponse {

  public static Component error(String message) {
    return getPrefix()
            .append(Component.text(message, NamedTextColor.RED));
  }

  public static Component info(String message) {
    return getPrefix()
            .append(Component.text(message, NamedTextColor.WHITE));
  }

  // alias. because I'll likely forget that it's named "info" and not "log"
  public static Component log(String message) {
    return info(message);
  }

  public static Component success(String message) {
    return getPrefix()
            .append(Component.text(message, NamedTextColor.GREEN));
  }

  public static Component getPrefix() {
    return Component.text("[", NamedTextColor.DARK_RED)
            .append(Component.text("DiscordLink", NamedTextColor.WHITE))
            .append(Component.text("] ", NamedTextColor.DARK_RED));

  }

}
