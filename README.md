# Discord Link
Links discord users to minecraft players

[![Build Status](http://ci.akii.dev/buildStatus/icon?job=tomi-bridge)]()

## How to
1. ~~Grab the [latest successful build](https://ci.akii.dev/job/tomi-bridge/lastSuccessfulBuild/artifact/target/tomi-bridge.jar) from the jenkins server~~ ci down atm
2. Drop it in the `plugins/` folder of your **paper** minecraft server. (it must be paper. (I think. (if you aren't using paper, why?)))
3. Run the server once to let the plugin generate all the necessary config files
4. Stop the server
5. Edit the plugin config in `plugins/DiscordLink/config.yml` and fill in all the necessary values
6. Start the server!

**You don't need to have whitelist enabled in order for this plugin to work.** In fact, if you do have it enabled, the plugin warns you to disable it. 
To link each player to their minecraft username, have them run `>whitelist <mc username>` in Discord. If they mess up, just go into the **plugin's** `whitelist.yml` (**not the server's**) and delete the entry.

## Dev to-do:
<details>
  <summary>Finished tasks</summary>

* [x] Create a config
* [x] Have users run a command on discord (`>link <mc username>`) and store that data for later retrieval
* [x] Keep users on discord from whitelisting themselves more than once
* [x] Add some more commands discord-side and server-side to configure the whitelist and death timers
  * [x] `/setlogger <level>` - sets the plugin's logger's verbosity level
* [x] Reloadable config
  * [x] `/reload`
  * [x] `>reload`
* [x] Create some pretty minecraft logging thing so it isn't just white text.
  * `[DiscordLink] &cThat action is not allowed!`
  * `[DiscordLink] &aUpdated successfully`
* [x] Dynamic minecraft command registering https://www.spigotmc.org/threads/is-there-any-way-to-write-to-the-plugin-yml-file-with-code.362432/
* [x] (low priority) create small utility function that formats seconds into human-readable time
* 100 seconds => "2 minutes" (1.666... rounded up)
* 3600 seconds => "1 hour"
* 1200 seconds => "1 day"
* and so on
</details>

* [ ] TODO: this https://www.spigotmc.org/wiki/stop-tabs-from-resetting-your-config-files/
* [ ] Create super-command on minecraft that houses all the other commands
  * `/dl reload`
  * `/dl setlogger`
  * `/dl xyz`
* [ ] Dynamic config editor
  copy this over to a minecraft command